package = totto

env = PKG_CONFIG_ALLOW_CROSS=1 # OPENSSL_INCLUDE_DIR=/usr/local/opt/openssl/include
cargo = $(env) cargo
rustc = $(env) rustc
debug-env = RUST_BACKTRACE=1 RUST_LOG=$(package)=debug
debug-cargo = $(env) $(debug-env) cargo
export CI_PIPELINE_ID ?= $(shell date +"%Y-%m-%d-%s")
export FPCO_CI_REGISTRY_IMAGE ?= registry.gitlab.fpcomplete.com/chrisallen/totto
export CI_REGISTRY_IMAGE ?= registry.gitlab.com/bitemyapp/totto
export FPCO_DOCKER_IMAGE ?= ${FPCO_CI_REGISTRY_IMAGE}:latest
export DOCKER_IMAGE ?= ${CI_REGISTRY_IMAGE}:latest
export KUBE_SPEC = etc/kubernetes/totto.yaml

build:
	$(cargo) build

version:
	@$(rustc) --version
	@$(cargo) --version

build-dynamic-release:
	$(cargo) build --release

build-static-release:
	$(cargo) build --target x86_64-unknown-linux-musl --release

build-dynamic-image:
	@docker build -t "${DOCKER_IMAGE}" -f ./etc/docker/dynamic/Dockerfile .
	@docker tag "${DOCKER_IMAGE}" "${FPCO_DOCKER_IMAGE}"

build-static-image:
	@docker build -t "${DOCKER_IMAGE}" -f ./etc/docker/static/Dockerfile .
	@docker tag "${DOCKER_IMAGE}" "${FPCO_DOCKER_IMAGE}"

push:
	@docker push "${DOCKER_IMAGE}"

push-fpco:
	@docker push "${FPCO_DOCKER_IMAGE}"

deploy:

run-docker:
	docker run -e TELEGRAM_BOT_TOKEN -t "${DOCKER_IMAGE}"

clippy:
	$(cargo) clippy

run: build
	./target/debug/$(package)

install:
	$(cargo) install --force

test:
	$(cargo) test

test-debug:
	$(debug-cargo) test -- --nocapture

fmt:
	$(cargo) fmt

watch:
	$(cargo) watch

# You need nightly for rustfmt at the moment
dev-deps:
	$(cargo) install fmt
	$(cargo) install clippy
	$(cargo) install rustfmt-nightly

.PHONY : build version build-dynamic-release build-static-release build-dynamic-image build-static-image push push-fpco run-docker clippy run install test test-debug fmt watch dev-deps
